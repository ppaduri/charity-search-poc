﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nest;

namespace elasticsearch_nest_webapi_angularjs.Models
{
    [Serializable]
    public class Charity
    {
        public int Id { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CharityName { get; set; }

        public string CharityMission { get; set; }

        public string CharityExternalKey { get; set; }
        public int CharityExternalKeyTypeId { get; set; }
        public int TagId { get; set; }
        public bool CanYcProcess { get; set; }
        public List<SourceBase> Sources;
        public Dictionary<String, String> Processors;
        public CharityAddress Address { get; set; }
    }
}