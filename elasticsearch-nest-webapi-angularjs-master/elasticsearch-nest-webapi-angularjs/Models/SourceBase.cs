﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace elasticsearch_nest_webapi_angularjs.Models
{
    public class SourceBase
    {
        public int SourceTypeId { get; set; }
        public string Name { get; set; }
        public Dictionary<String, String> CustomAttributes;
    }
}