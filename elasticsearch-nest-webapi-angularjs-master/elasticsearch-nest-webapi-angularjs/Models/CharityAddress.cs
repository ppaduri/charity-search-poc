﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace elasticsearch_nest_webapi_angularjs.Models
{
    public class CharityAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public int CountryId { get; set; }
        public string IsoCode { get; set; }
        public string CharityCountry { get; set; }
    }
}