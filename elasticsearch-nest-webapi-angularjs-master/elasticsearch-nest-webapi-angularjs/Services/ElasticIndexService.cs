﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Hosting;
using elasticsearch_nest_webapi_angularjs.Models;
using elasticsearch_nest_webapi_angularjs.Utils;
using Nest;

namespace elasticsearch_nest_webapi_angularjs.Services
{
    public class ElasticIndexService
    {
        private readonly IElasticClient client;

        public ElasticIndexService()
        {
            client = ElasticConfig.GetClient();
        }

        public void CreateIndex(string fileName, int maxItems)
        {
           
            client.DeleteIndex(ElasticConfig.IndexName);
            if (!client.IndexExists(ElasticConfig.IndexName).Exists)
            {
                //var indexDescriptor = new CreateIndexDescriptor(ElasticConfig.IndexName)
                //    .Mappings(ms => ms
                //        .Map<Post>(m => m.AutoMap()));

                var indexDescriptor = new CreateIndexDescriptor(ElasticConfig.IndexName)
                   .Mappings(ms => ms
                       .Map<Charity>(m => m.AutoMap()));
                client.CreateIndex(ElasticConfig.IndexName, i=> indexDescriptor);
            }

            BulkIndex(HostingEnvironment.MapPath("~/data/" + fileName), maxItems);
        }

        private IEnumerable<Post> LoadPostsFromFile(string inputUrl)
        {
            using (
                var sqlConnection = new SqlConnection("Server=250.190.11.242;Database=YourCause;UID=yctest;PWD=yctest;"))
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("select top 250 zCharityId,zName from tblCharity", sqlConnection);

                // 2. Call Execute reader to get query results
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Post post = new Post
                    {
                        Id = Convert.ToString(rdr.GetValue(0)),
                        Title = Convert.ToString(rdr.GetValue(1)),
                        CreationDate = DateTime.Now,
                        Score = 1,
                        Body = "Body",
                        Tags =
                            null,
                        AnswerCount =
                            1
                    };
                    post.Suggest = post.Tags;
                    yield return post;
                }
                //                }
                //            }
                //        }
                //    }
            }
        }
        private IEnumerable<Charity> LoadCharities()
        {
            using (
                var sqlConnection = new SqlConnection("Server=10.10.141.50;Database=dbods;UID=ssrs_user;PWD=YourCause13800;"))
            {
                sqlConnection.Open();
               // SqlCommand cmd = new SqlCommand("select top 1000 * from tblCharity where zFoundationCode is not NULL and zNTEECode is not null and zIRSSubSection is not null  order by zCharityID desc", sqlConnection);
                string gsStatement = "select top 250 CHD.zCharityID,CHD.zCharityName,CHY.zMission as zCharityMission,CHD.zCharityExternalKeyTypeID,CHD.zCharityExternalKey,CHD.zFoundationCode,CHD.zIRSSubSection,CHD.zNTEECode,CHY.zTagID from dbods.dbo. tblCharity CHD join YourCause.dbo.tblCharity CHY on CHD.zCharityID= CHY.zCharityID where CHD.zCharityExternalKeyTypeID=1 order by CHD.zCharityID DESC";
                string canadaHelps = "select top 250 CHD.zCharityID,CHD.zCharityName,CHY.zMission as zCharityMission,CHD.zCharityExternalKeyTypeID,CHD.zCharityExternalKey,CHD.zFoundationCode,CHD.zIRSSubSection,CHD.zNTEECode,CHY.zTagID from dbods.dbo. tblCharity CHD join YourCause.dbo.tblCharity CHY on CHD.zCharityID= CHY.zCharityID where CHD.zCharityExternalKeyTypeID=2 order by CHD.zCharityID DESC";
                string ammado = "select top 250 CHD.zCharityID,CHD.zCharityName,CHY.zMission as zCharityMission,CHD.zCharityExternalKeyTypeID,CHD.zCharityExternalKey,CHD.zFoundationCode,CHD.zIRSSubSection,CHD.zNTEECode,CHY.zTagID from dbods.dbo. tblCharity CHD join YourCause.dbo.tblCharity CHY on CHD.zCharityID= CHY.zCharityID where CHD.zCharityExternalKeyTypeID=4 order by CHD.zCharityID DESC";
                string ncseSchools = "select top 250 CHD.zCharityID,CHD.zCharityName,CHY.zMission as zCharityMission,CHD.zCharityExternalKeyTypeID,CHD.zCharityExternalKey,CHD.zFoundationCode,CHD.zIRSSubSection,CHD.zNTEECode,CHY.zTagID from dbods.dbo. tblCharity CHD join YourCause.dbo.tblCharity CHY on CHD.zCharityID= CHY.zCharityID where CHD.zCharityExternalKeyTypeID=5 order by CHD.zCharityID DESC";
                string gsProcessor = "select * from vwCharityProcessorMapCurrent where zCharityID in(select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=1 order by zCharityID DESC)";
                string csProcessor = "select * from vwCharityProcessorMapCurrent where zCharityID in(select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=2 order by zCharityID DESC)";
                string amamdoProcessor = "select * from vwCharityProcessorMapCurrent where zCharityID in(select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=4 order by zCharityID DESC)";
                string ncseProcessor = "select * from vwCharityProcessorMapCurrent where zCharityID in(select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=5 order by zCharityID DESC)";
                string gsSource =
                    "select XR.zCharityID,XR.zCharitySourceTypeID,S.zCharitySourceTypeDesc from tblCharitySourceXRef XR JOIN tblCharitySourceType S on XR.zCharitySourceTypeID= S.zCharitySourceTypeID where XR.RecordStatus=1and  zCharityID in( select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=1 order by zCharityID DESC)";
                string chSource =
                  "select XR.zCharityID,XR.zCharitySourceTypeID,S.zCharitySourceTypeDesc from tblCharitySourceXRef XR JOIN tblCharitySourceType S on XR.zCharitySourceTypeID= S.zCharitySourceTypeID where XR.RecordStatus=1and  zCharityID in( select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=2 order by zCharityID DESC)";
                string ammadoSource =
                  "select XR.zCharityID,XR.zCharitySourceTypeID,S.zCharitySourceTypeDesc from tblCharitySourceXRef XR JOIN tblCharitySourceType S on XR.zCharitySourceTypeID= S.zCharitySourceTypeID where XR.RecordStatus=1and  zCharityID in( select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=4 order by zCharityID DESC)";
                string ncseSource =
                  "select XR.zCharityID,XR.zCharitySourceTypeID,S.zCharitySourceTypeDesc from tblCharitySourceXRef XR JOIN tblCharitySourceType S on XR.zCharitySourceTypeID= S.zCharitySourceTypeID where XR.RecordStatus=1and  zCharityID in( select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=5 order by zCharityID DESC)";
                string gsaddressStatement =
                   "select zCharityID,zAddress1,zAddress2,zAddress3,zCity,zStateOrProvince,zPostalCode,zCountryID,zIsoCode,CharityCountry from vwCharityAddressCurrent where zCharityID in (select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=1 order by zCharityID DESC)";
                string CHaddressStatement =
                "select zCharityID,zAddress1,zAddress2,zAddress3,zCity,zStateOrProvince,zPostalCode,zCountryID,zIsoCode,CharityCountry from vwCharityAddressCurrent where zCharityID in (select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=2 order by zCharityID DESC)";
                string AmamdoaddressStatement =
                "select zCharityID,zAddress1,zAddress2,zAddress3,zCity,zStateOrProvince,zPostalCode,zCountryID,zIsoCode,CharityCountry from vwCharityAddressCurrent where zCharityID in (select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=4 order by zCharityID DESC)";
                string ncseaddressStatement =
                "select zCharityID,zAddress1,zAddress2,zAddress3,zCity,zStateOrProvince,zPostalCode,zCountryID,zIsoCode,CharityCountry from vwCharityAddressCurrent where zCharityID in (select top 250 zCharityID from tblCharity where zCharityExternalKeyTypeID=5 order by zCharityID DESC)";
                // 2. Call Execute reader to get query results
               // SqlDataReader rdr = cmd.ExecuteReader();
            DataSet ds = new DataSet();
             
            // Fill Parent Table in the DataSet
            SqlDataAdapter da = new SqlDataAdapter(gsStatement, sqlConnection);
            da.Fill(ds, "Charities");

            da = new SqlDataAdapter(canadaHelps, sqlConnection);
            da.Fill(ds, "Charities");

            da = new SqlDataAdapter(ammado, sqlConnection);
            da.Fill(ds, "Charities");

            da = new SqlDataAdapter(ncseSchools, sqlConnection);
            da.Fill(ds, "Charities");
            // Fill Child Table in the DataSet
            da = new SqlDataAdapter(gsProcessor, sqlConnection);
            da.Fill(ds, "CharityProcessors");
            da = new SqlDataAdapter(csProcessor, sqlConnection);
            da.Fill(ds, "CharityProcessors");
            da = new SqlDataAdapter(amamdoProcessor, sqlConnection);
            da.Fill(ds, "CharityProcessors");
            da = new SqlDataAdapter(ncseProcessor, sqlConnection);
            da.Fill(ds, "CharityProcessors");
 da = new SqlDataAdapter(gsSource, sqlConnection);
            da.Fill(ds, "Sources");
            da = new SqlDataAdapter(chSource, sqlConnection);
            da.Fill(ds, "Sources");
            da = new SqlDataAdapter(ammadoSource, sqlConnection);
            da.Fill(ds, "Sources");
            da = new SqlDataAdapter(ncseSource, sqlConnection);
            da.Fill(ds, "Sources");
            da = new SqlDataAdapter(gsaddressStatement, sqlConnection);
            da.Fill(ds, "Addresses");
            da = new SqlDataAdapter(CHaddressStatement, sqlConnection);
            da.Fill(ds, "Addresses");
            da = new SqlDataAdapter(AmamdoaddressStatement, sqlConnection);
            da.Fill(ds, "Addresses");
            da = new SqlDataAdapter(ncseaddressStatement, sqlConnection);
            da.Fill(ds, "Addresses");
                 for (int i = 0; i < ds.Tables["Charities"].Rows.Count;i++ )
            {
                DataRow charityDataRow = ds.Tables["Charities"].Rows[i];
                      Charity charity = new Charity
                    {
                        Id = Convert.ToInt32(charityDataRow["zCharityID"]),
                        CharityName = Convert.ToString(charityDataRow["zCharityName"]),
                        CharityMission = Convert.ToString(charityDataRow["zCharityMission"]),
                        CreationDate = DateTime.Now,
                        CharityExternalKeyTypeId = Convert.ToInt32(charityDataRow["zCharityExternalKeyTypeID"]),
                        CharityExternalKey = Convert.ToString(charityDataRow["zCharityExternalKey"]),
                        TagId = Convert.ToInt32(charityDataRow["zTagID"])
                       
                    };
                     charity.Sources = new List<SourceBase>();
                foreach (DataRow sourceRow in ds.Tables["Sources"].Select("zCharityId=" + charity.Id))
                {
                    SourceBase sr = new SourceBase
                    {
                        SourceTypeId = Convert.ToInt32(sourceRow["zCharitySourceTypeID"]),
                        Name = Convert.ToString(sourceRow["zCharitySourceTypeDesc"])

                    };
                    if (sr.SourceTypeId == 8)//guidestar. For now we know custom attributes for guidestar only.
                    {
                        sr.CustomAttributes = new Dictionary<String, String>();
                        sr.CustomAttributes.Add("FoundationCode", Convert.ToString(charityDataRow["zFoundationCode"]));
                        sr.CustomAttributes.Add("IRSSubSection", Convert.ToString(charityDataRow["zIRSSubSection"]));
                        sr.CustomAttributes.Add("NTEECode", Convert.ToString(charityDataRow["zNTEECode"]));
                      
                    }
                    charity.Sources.Add(sr);
                }

                charity.Processors = new Dictionary<String, String>(); 
                    foreach (DataRow processorRow in ds.Tables["CharityProcessors"].Select("zCharityId=" + charity.Id))
                {
                    charity.Processors.Add(Convert.ToString(processorRow["zCharityProcessorTypeID"]), Convert.ToString(processorRow["zCharityProcessorType"]));     
                }
                if (charity.Processors.ContainsKey("1"))// yourcause processor
                {
                    charity.CanYcProcess = true;
                }
                foreach (DataRow addressRowRow in ds.Tables["Addresses"].Select("zCharityId=" + charity.Id))
                {
                    charity.Address = new CharityAddress
                    {

                        Address1 = Convert.ToString(addressRowRow["zAddress1"]),
                        Address2 = Convert.ToString(addressRowRow["zAddress2"]),
                        City = Convert.ToString(addressRowRow["zCity"]),
                        State = Convert.ToString(addressRowRow["zStateOrProvince"]),
                        PostalCode = Convert.ToString(addressRowRow["zPostalCode"]),
                        CharityCountry = Convert.ToString(addressRowRow["CharityCountry"]),
                        IsoCode = Convert.ToString(addressRowRow["zIsoCode"]),
                        CountryId = Convert.ToInt32(addressRowRow["zCountryID"])


                    };
                }
                yield return charity;
            }
                //while (rdr.Read())
                //{
                //    Charity charity = new Charity
                //    {
                //        Id = Convert.ToInt32(rdr["zCharityID"]),
                //        CharityName = Convert.ToString(rdr["zCharityName"]),
                //        CharityMission = Convert.ToString(rdr["zCharityMission"]),
                //        CreationDate = DateTime.Now,
                //        CharityExternalKeyTypeId = Convert.ToInt32(rdr["zCharityExternalKeyTypeID"]),
                //        CharityExternalKey = Convert.ToString(rdr["zCharityExternalKey"])
                        
                       
                //    };
                //     charity.Sources = new List<SourceBase>();
                //    SourceBase gs = new SourceBase
                //    {
                //        SourceId = 1,
                //        Name = "Guidestar"

                //    };
                //    gs.CustomAttributes = new Dictionary<String, String>(); ;
                //    gs.CustomAttributes.Add("FoundationCode", Convert.ToString(rdr["zFoundationCode"]));
                //    gs.CustomAttributes.Add("IRSSubSection", Convert.ToString(rdr["zIRSSubSection"]));
                //    gs.CustomAttributes.Add("zNTEECode", Convert.ToString(rdr["zNTEECode"]));                  
                //    charity.Sources.Add(gs);
                //    yield return charity;
                }
                //                }
                //            }
                //        }
                //    }
            }
        private void BulkIndex(string path, int maxItems)
        {

            int i = 0;
            int take = maxItems;
            int batch = 2;
            //foreach (var batches in LoadPostsFromFile(path).Take(take).Batch(batch))
            //{
            //    i++;
            //    var result = client.IndexMany<Post>(batches, "stackoverflow");
            //}
            foreach (var batches in LoadCharities().Take(take).Batch(batch))
            {
                i++;
                var result = client.IndexMany<Charity>(batches, ElasticConfig.IndexName);
            }
            
        }
    }
}